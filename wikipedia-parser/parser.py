#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 25 22:49:47 2020

@author: varpu
"""

from bs4 import BeautifulSoup
from urllib.request import urlopen
from queue import Queue
import re
from threading import Thread
import wikipedia

class wikiPage:
    def __init__(self, link, title, predecessor,degree=0):
        self.link = link
        self.title = title
        self.predecessor = predecessor
        self.degree = degree

#function to extract the link to a new wikipage
def extract_url(url):
    new_url = re.search(r"\/wiki\/(.+)$",url)
    if new_url:
        new_url = new_url.group(1)
    return new_url

#function to check if an hyperlink is valid
def link_is_valid(hyperlink):
    #here we check if the hyperlink exists and if it begind the right way
    if hyperlink.get('href') and hyperlink.get('href')[:6] == "/wiki/":
        #we check if the hyperlink is containing another HTML element or if the hyperlink is an internal file, if so, the link is not valid
        if (hyperlink.contents and str(hyperlink.contents[0])[0] != "<" and ":" not in hyperlink.get('href')):
            return True       
    else: 
        return False


#function to stock the path between the two pages 
def stock_path(current_page):
    path = []
    #we add every predecessor of the current page to the path list
    while current_page.predecessor:
        path.append((current_page.link, current_page.title))
        #the current_page becames the predecessor page 
        current_page = current_page.predecessor
    path.append((current_page.link, current_page.title))
    #we return the path list in reverse order to have the path from the starting page to the end page
    path = path[::-1]
    return path



#function to check if the links are matching
def check_links(links, visited, queue, destination_page, wikipage):
    
    for link, title in links:
        #we check if the link has already been visited, if not it is added to the visited pages and in the queue (in case it is not the destination page)
        if link not in visited:
            if link == destination_page:
                #we save the path made to reach the destination page
                pages = stock_path(wikipage)
                pages.append((link, title))
                return pages
            visited.add(link)
            queue.put(wikiPage(link, title, wikipage, wikipage.degree + 1))
            

  
    
#function to parse the wikipedia page to find the hyperlinks
def get_all_links(wikipage, connections,dictionary):
    #list to stock the internal links as tuples with the wikipage and the url
    links = []
    #list to contain the names of wikipages
    names = []
    i = 0
    #list to stock all the links contained in one wikipage
    internal_links = []
    
    current_conn = urlopen("https://en.wikipedia.org/wiki/" + wikipage.link)
    
    #we add current connection to connection list
    connections.append((wikipage,current_conn))

    #we print the path that lead to every hyperlinkg being processed 
    for res in stock_path(wikipage):
        i += 1
        names.append(str (i)+". " + res[1])
    print(*names, sep = " --> ")
        
    #list to stock all the internal links of a wikipedia page
    internal_links = []

    #we want only the text content 
    soup = BeautifulSoup(current_conn,"html.parser").find('div',{'id': 'mw-content-text'})
    

    #we search for every paragraph of the wikipage 
    for paragraph in soup.find_all('p'):
        #we search for each hyperlink in each paragraph
        for hyperlink in paragraph.find_all('a'):
            #first we have to check if the link is valid
            validity = link_is_valid(hyperlink)
            if validity == True:
                #if the link is valid, it is added to the list
                internal_links.append(hyperlink)
                
    
    links =  [(hyperlink.get('href')[6:], hyperlink.contents[0]) for hyperlink in internal_links]
    
    #dictionary to get a list of links for each page
    dictionary.update([(wikipage, links)])
    
    
        
    
#function to start the search for the path
def find_path(start, destination):
    visited_pages = set()
    #Global queue
    queue = Queue()
    worker_nb = 200
    
    #we put the starting page to the queue
    queue.put(wikiPage(extract_url(start), extract_url(start), None))
    destination_page = extract_url(destination)
    current_degree = 0
    web_links = []
    while True:
        #there's an idea that wikipedia articles are not linked by a path of more than 6 degrees 
        if current_degree > 7:
            print("Degree too high")
            return "None"
        
        elif queue.empty() != True:
            page = queue.get()

        elif queue.empty() == True or page.degree > current_degree:
            if  queue.empty() != True:
                current_degree += 1

            while web_links:
                connections = []
                threads = []
                links = []
                dictionary = {}
                #Creating the threads for each worker
                for i in range(worker_nb):
                    if web_links:
                        t=Thread(target=get_all_links, args=(web_links.pop(),connections,dictionary))
                        t.start()
                        threads.append(t)
                        
                for i in range(len(threads)):
                    threads[i].join()
                    
                #we iterate through every connection to see if the result is reached    
                for connection in connections:
                    links = dictionary.get(connection[0])
                    result = check_links(links, visited_pages, queue, destination_page, connection[0])
                    if result:
                        return result

            web_links = []

        web_links.append(page)
        continue
        result = check_links(links, visited_pages, queue, destination_page, page)
        if result:
            return result
        
#function to define the choice between a word and a link to define wiki pageg
def choose(choice):
    #if the users wants to give direct links
    if choice == "1":
        start_link = input ("Give the wikipedia link for starting page: ")
        end_link = input ("Give the wikipedia link for ending page: ")
        result = find_path(start_link, end_link)
        return result
        
     #if the user wants to give title to perform the search directly in wikipedia      
    elif choice == "2":
        print("\nWARNING: Please beware that this method may not find every existing wikipedia pages and providing the URLs may work better for some pages.\n")
       
        try:
            #we take the starting page name and the ending page name as inputs from user
            start_point = wikipedia.page(input("Give the name of the wikipedia page you want to use as a starting point: "))
            end_point = wikipedia.page(input("Give the name of the wikipedia page you want to use as an end point: "))
            #we fecth the links using the wikipedia library 
            start_link = start_point.url
            end_link = end_point.url
        except:
            #if an error occurs program tells the user that the page wasn't found 
            print("The matching wikipedia page wasn't found. You can retry this way or give the links.\n")
            return None 
            
        result = find_path(start_link, end_link)
        return result
        
    
    else:
        print("Invalid choice !\n")
        

def main():
    while True:
        
        #we ask the user if he wants to give a link to the wikipage or a name and the link will be searched using the wikipedia library
        choice = input("Choose between giving a link (1) or a wikipedia page name (2): ")
        result = choose(choice)  
            
        i = 0
        #when the result is found, we print it to the user 
        if result:
            print("\n The solution is: \n")
            for res in result:
                i += 1
                print(str(i) + ". " + res[1])
            break
            

if __name__ == "__main__":
    main()
    

