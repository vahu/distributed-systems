#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 23 00:09:00 2020

@author: varpu
"""

import pickle
from timeit import default_timer as timer
import random
import sys
import json
import msgpack
import yaml 
import os
from dicttoxml import dicttoxml
import xmltodict

#function to create dictionary
def create_data(number):
    #a list of words that will be used as elements in the dictionnary
    myData = ['weird','bonjour','name','nimi','nom','choice','word','random','stuff','aleatoir','ranska','suomi','f','word','mot','sana']
    #we create a data dictionnary
    data = {}
    #we add values in the dictionnary named data
    for i in range (int(number)):
        data[str(i)] = random.choice(myData)
    return data

#function to get the data of a data object    
def size_fun(data):
    size = sys.getsizeof(data)
    print("The size of the data is "+ str(size) + " bytes or "+ str(round(size * (10 ** (-6) ),2)) + " megabytes. ")
    

#we take the number of elements wanted in the dictionary from the user     
input_number = input("Give how many entries you want in the serializable data: ")
data = create_data(input_number)

size_fun(data)

#We run the program until the user's chooses to quit
while True:
    choice = input ("You have the choice between 0) Quit, 1) Native, 2) XML, 3) JSON, 4) MessagePack or 5) YAML.: ")
   
    
    if choice == "Native" or choice == "1":
        #for native using pickle
        selection = input("Choose 1 to serialize, 2 to deserialize: ")
        
        #selection if we want to serialize some data
        if selection == "1":
            #we start the timer to measure the time the serialization takes
            start = timer()
            pickle_out = open ("data.pickle","wb")
            pickle_data = pickle.dump(data,pickle_out)
            end = timer()
            pickle_out.close()
            print(str (end-start) + " seconds or " + str(round((end-start)*1000,5)) + " milliseconds.")
            #we get the size of the file 
            size = os.path.getsize('data.pickle')
            print("The serialized file has a size of " + str(size) + " bytes or " + str(round(size * (10 ** (-6) ),2)) + " megabytes. ")
            
            
        #selection if we want to deserialize 
        elif selection == "2":
            try:
                start = timer()
                pickle_in = open("data.pickle","rb")
                pickle_data = pickle.load(pickle_in)
                end = timer()
                pickle_in.close()
                print(str (end-start) + " seconds or " + str(round((end-start)*1000,5)) + " milliseconds.")
            except IOError:
                print("Error: The file does not exist.\n")
        else:
            print("Invalid choice.\n")
            
    elif choice == "XML" or choice == "2":
        #for XML using dicttoxml to serialize and xmltodict to deserialize
        selection = input("Choose 1 to serialize, 2 to deserialize: ")
        
        #selection if we want to serialize some data
        if selection == "1":
            #we start the time to measure the time the serialization takes
            start = timer()
            xml_data = dicttoxml(data)
            xml_out = open("data.xml","w")
            
            xml_out.write(xml_data.decode())
            end = timer()
            
            print(str (end-start) + " seconds or " + str(round((end-start)*1000,5)) + " milliseconds.")
            xml_out.close()
            
            size = os.path.getsize('data.xml')
            print("The serialized file has a size of " + str(size) + " bytes or " + str(round(size * (10 ** (-6) ),2)) + " megabytes. ")
            
            
        #selection if we want to deserialize XML to dictionnary
        elif selection == "2":
            try:
                #starting the timer
                start = timer()
                xml_in = open("data.xml","r")
                xml_data = xmltodict.parse(xml_in.read())
                #ending the timer
                end = timer()
                xml_in.close()
                print(str (end-start) + " seconds or " + str(round((end-start)*1000,5)) + " milliseconds.")
            except IOError:
                print("Error: The file does not exist.\n")
        else:
            print("Invalid choice.\n")
        
            
    elif choice == "JSON" or choice == "3":
        #for JSON using json library
        selection = input("Choose 1 to serialize, 2 to deserialize: ")
        #selection if we want to serialize some data
        if selection == "1":
            #we start the time to measure the timer the serialization takes
            start = timer()
            json_out = open ("data.json","w")
            json.dump(data,json_out)
            end = timer()
            json_out.close()
            print(str (end-start) + " seconds or " + str(round((end-start)*1000,5)) + " milliseconds.")
            size = os.path.getsize('data.json')
            print("The serialized file has a size of " + str(size) + " bytes or " + str(round(size * (10 ** (-6) ),2)) + " megabytes. ")
                
        #selection if we want to deserialize JSON to dictionnary
        elif selection == "2":
            try:
                start = timer()
                json_in = open("data.json","r")
                json_data = json.load(json_in)
                end = timer()
                json_in.close()
                print(str (end-start) + " seconds or " + str(round((end-start)*1000,5)) + " milliseconds.")
            except IOError:
                print("Error: The file does not exist.\n")
            
            
        else:
            print("Invalid choice.\n")
        
    elif choice == "MessagePack" or choice == "4":
        #MessagePack using msgpack library
        selection = input("Choose 1 to serialize, 2 to deserialize: ")
        #selection if we want to serialize some data
        if selection == "1":
            #we start the time to measure the time the serialization takes
            start = timer()
            msgpack_out = open ("data.msgpack","wb")
            msgpack.pack(data,msgpack_out)
            end = timer()
            msgpack_out.close()
            print(str (end-start) + " seconds or " + str(round((end-start)*1000,5)) + " milliseconds.")
            size = os.path.getsize('data.msgpack')
            print("The serialized file has a size of " + str(size) + " bytes or " + str(round(size * (10 ** (-6) ),2)) + " megabytes. ")
                
        #selection if we want to deserialize messagepack to dictionnary
        elif selection == "2":
            try:
                start = timer()
                msgpack_in = open("data.msgpack","rb")
                msgpack_data = msgpack.unpack(msgpack_in)
                end = timer()
                msgpack_in.close()
                print(str (end-start) + " seconds or " + str(round((end-start)*1000,5)) + " milliseconds.")
            except IOError:
                print("Error: The file does not exist.\n")
                    
        else:
            print("Invalid choice.\n")
            
    elif choice == "YAML" or choice == "5":
        #YAML using yaml library
        selection = input("Choose 1 to serialize, 2 to deserialize: ")
        #selection if we want to serialize some data
        if selection == "1":
            #we start the time to measure the time the serialization takes
            start = timer()
            yaml_out = open ("data.yaml","w")
            yaml.dump(data,yaml_out)
            end = timer()
            yaml_out.close()
            print(str (end-start) + " seconds or " + str(round((end-start)*1000,5)) + " milliseconds.")
            size = os.path.getsize('data.yaml')
            print("The serialized file has a size of " + str(size) + " bytes or " + str(round(size * (10 ** (-6) ),2)) + " megabytes. ")
                
        #selection if we want to deserialize YAML to dictionnary
        elif selection == "2":
            try:
                start = timer()
                yaml_in = open("data.yaml","r")
                yaml_data = yaml.load(yaml_in)
                end = timer()
                yaml_in.close()
                print(str (end-start) + " seconds or " + str(round((end-start)*1000,5)) + " milliseconds.")
            except IOError:
                print("Error: The file does not exist.\n")

        else:
            print("Invalid choice.\n")
            
    #choice when the user wants to quit
    elif choice == "Quit" or choice == "quit" or choice == "0":
        print("Thank you for using the program!\n")
        break
    #if the user choice is not valid
    else:
        print("Invalid choice, try again.\n")
        continue