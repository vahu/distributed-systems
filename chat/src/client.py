#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 17:05:04 2020

@author: varpu
"""
import socket
import select
import errno
import sys
import pdb
from classes import Server, Channel, Client
import classes 
 
HEADER_LENGTH = 10
BUFFER = 4096



def main(): 
    
    IP = "127.0.0.1"
    
    client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    try:
        client_socket.connect((IP,classes.PORT))
    except:
        print("A connection error occured")
        sys.exit()
        
    print("You have succesfully connected to server!")
    
    client_socket_list = [sys.stdin,client_socket]
        
    while True:
        read_sockets, write_sockets,exception_sockets = select.select(client_socket_list, [] ,client_socket_list)

        for sock in read_sockets:
            if sock == client_socket:
                message = sock.recv(BUFFER)
                
                if message: 
                    if message == 'quit':
                        sys.stdout.write('Thank you for using the server\n')
                        sys.exit(2)
                    else:
                        sys.stdout.write(message.decode())
                        if 'nickname' in message.decode():
                            message_prefix = 'nickname: '
#                        elif 'private' in message.decode():
#                            message_prefix = 'private message: '
                        else:
                            message_prefix = ''
                        print(' >> ' )
            
        else:
            message = message_prefix + sys.stdin.readline()
            client_socket.sendall(message.encode())
                        
                

if __name__ == "__main__":
    main()