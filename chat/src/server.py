#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 17:00:20 2020

@author: varpu
"""

import socket 
import select
import sys
from classes import Server, Channel, Client
import classes  


HEADER_LENGTH = 10

#we set the maximum input size to prevent user from making a too big input
BUFFER = 4096

def main():
    start_server()

#function to start the server    
def start_server():
    IP = "127.0.0.1"
    
    #we utilize the socket creation function from classes.py
    serv = classes.create_socket(IP)

  
    new_server = Server()
    #list to handle the different sockets
    sockets = []
    sockets.append(serv)
    
    

    
    #we accept connections as long as the server is running
    while True:
        
        read_clients, write_clients,exception_sockets = select.select(sockets, [] ,sockets)
        for current_client in read_clients:
            if current_client == serv:
                client_socket, add = current_client.accept()

                new_client = Client(client_socket)
                sockets.append(new_client)
                new_server.newbie(new_client)
           
            else: 
                #we take the message from the client
                message = current_client.socket.recv(BUFFER)
                if message:
                    message = message.decode()
                    new_server.receive_message(current_client,message)
                #when a user closes the window it logs him/her of automatically
                else:
                    current_client.socket.close()
                    sockets.remove(current_client)
                    print(current_client.name + " has left the server.\n")
                      
        for current_socket in exception_sockets:
            current_socket.close()
            sockets.remove(current_socket)



if __name__ == "__main__":
    main()

