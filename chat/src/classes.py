#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 21:16:57 2020

@author: varpu
"""

import socket
import sys



PORT = 8080

#A function to initialize the server
def create_socket(IP):
    
    
    #creation of the socket for the server
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    sock.setblocking(0)
    
    try:
    #we bind the server to the defined IP and PORT so that they can't be used by something else
        sock.bind((IP, PORT))
    except:
        print("Couldn't bind. Error : " + str(sys.exc_info()))
        sys.exit()
    sock.listen()
    print("Server is now listening")
    return sock
    
#the class to handle the server
class Server: 
    def __init__(self):
        self.channels = {}
        self.channel_client = {}
        self.client_list = {}
        self.private_clients = {}
        
    #function to fetch the user's nickname
    def newbie (self,new_client):
        new_client.socket.sendall(b'Looks like you are new here. What\'s your nickname? ')
        
    #function to list the different channels to the client    
    def list_channels (self, client):
        i = 0
        #we check if there are existing channels on the server, if so we list every channel and how many people are connected to the user 
        if len(self.channels) == 0:
            message = 'There is no active channel. You can create your own by writing [channel channel_name]'
            client.socket.sendall(message.encode())
        else:
            message = "There is a list of the existing channels:"
            for channel in self.channels:
                i += 1
                message += "Channel "+ str (i) + " is named " + channel + " and has " + str(len(self.channels[channel].clients)) + " connected clients.\n"
            client.socket.sendall(message.encode())
            
    #function to list the different active users so that the user can select who to chat with privately
    def list_clients (self, current_client):
        i = 0
        print(str(len(self.client_list)))
        #if there is only one active client it is the user asking for the list
        if len(self.client_list) == 1:
            message = 'There is only one active client which is you, please wait for other clients.\n'
            current_client.socket.sendall(message.encode())
        else: 
            message = 'Here is a list of current clients: \n'
            for client in self.client_list:
                print(self.client_list[client].name)
                i += 1
                message += "Client " + str (i) + " is " + self.client_list[client].name + ".\n"
            current_client.socket.sendall(message.encode())
            
     #function to remove clients to delete the user from a specific channel and from the list that keeps the names of users enrolled in channels 
    def delete_client(self,client):
        if client.name in self.channel_client:
            self.channels[self.channel_client[client.name]].delete_client(client)
            del self.channel_client[client.name]
        print(client.name + " left the chat\n")
        
    def send_private(self,name,message):
        
        name_exists = False
        
        for client in self.client_list:
            if name == self.client_list[client].name:
                private_client = self.client_list[client].socket
                private_client.socket.sendall(message.encode())
                name_exists = True
        if name_exists == False:
            return None 
           
                
                
        
    #function to receiver messages
    def receive_message(self, client,message):
        #the user has different choices to perform
        choices = b'You have different choices:\n'\
        +b'option 1: [list] to get the list of the channels\n'\
        +b'option 2: [channel channel_name] to join a specified channel\n'\
        +b'option 3: [users] to get the list of all the connected users\n'\
        +b'option 4: [private client_name message] to send a private message to a client'\
        +b'option 5: [quit] to quit the channel\n'\
        

        print(client.name + "says: " + message)
        
        #we check if the user wants to see the different channels
        if "list" in message:
            self.list_channels(client)
        #we check if the message is the first one with the nickname of the user
        elif "nickname:" in message:
            name = message.split()[1]
            client.name = name
            new_client = Client(client,name)
            self.client_list[name] = new_client
            print(client.name + " has connected to the chat.\n")
            client.socket.sendall(choices)
        
        #we check if the user wants to join a channel
        elif "channel" in message:
            same_channel = False
            
            #we control if the client has entered just one argument alongside the command
            if len(message.split()) == 2:
                channel_name = message.split()[1]
                if client.name in self.channel_client:
                    if self.channel_client[client.name] == channel_name:
                        client.socket.sendall(b'You are already chatting in this channel.')
                        #same_channel = True 
                    else:
                        left_channel = self.channel_client[client.name]
                        self.channels[left_channel].delete_client(client)
                        
                else:
                    #if the channel already exists, we add the new client into it
                    if channel_name in self.channels: 
                        self.channels[channel_name].clients.append(client)
                        self.channel_client[client.name] = channel_name
                    #if the channel doesn't exist yet, we create it
                    else:
                        new_channel = Channel(channel_name)
                        self.channels[channel_name] = new_channel
                        self.channels[channel_name].clients.append(client)
                        self.channel_client[client.name] = channel_name
                
            else: 
                client.socket.sendall(b'A parameter error has occured. Please retry.\n'+choices)
        #we list the different users to the current user
        elif "users" in message:
            self.list_clients(client)
        elif "private" in message:
            if len(message.split()) >=2:
                private_nickname = message.split()[1]
                #we parse the message to get only the relevant part for the other client
                new_message = ' '.join(message.split()[2:])
                sending = client.name + ' sent \" ' + new_message+'\". Use [private ' + client +' message] to respond.'
                print(new_message)
                private_client = self.send_private(private_nickname,sending)
                if private_client == None:
                    client.socket.sendall(b'The client doesn\'t exist')
                else: 
                    client.socket.sendall(b'Message sent to' + private_nickname)
#                    
                    
            else:
                client.socket.sendall(b'A parameter error has occured. Please retry.\n' + choices)
                
        #allows the user to quit a channel
        elif "quit" in message: 
            client.socket.sendall("Leaving the chat".encode())
            self.delete_client(client)
        #we check if the user is already in a conversation, if not we send him a message to either join a new channel or to send private messages    
        else:
            if client.name in self.channel_client:
                self.channels[self.channel_client[client.name]].broadcast_message(client,message.encode())
            else:
                message = 'You are not in a channel to chat!\n'\
                + 'you can send a private message to a client by typing [private client_name message]\n'\
                + 'or you can join a channel where to chat by entering [channel channel_name]\n'
                client.socket.sendall(message.encode())
            
   
 #class for the client       
class Client:
    def __init__(self, socket, name ="new"):
        self.socket = socket
        self.name = name
        
    def fileno(self):
        return self.socket.fileno()
    
    def private_msg(self,client,message):
        client.socket.sendall(message)
        
        
    
 #class for the channel   
class Channel:
    def __init__ (self,name):
        self.clients = []
        self.name = name
    #function to send the message to the people enrolled in the same channel
    def broadcast_message(self, client, message):
        message = client.name.encode() + b":"+message
        for client in self.clients:
            client.socket.sendall(message)
    #function to delete the client from the channel and to tell the other members that a client has left the chat        
    def delete_client(self,client):
        self.clients.remove(client)
        leaving_message = client.name.encode() +b"left the chat."
        self.broadcast_message(client, leaving_message)
    
    